import os,json
from tabulate import tabulate
import pinyin
import xlsxwriter

PATH=os.path.dirname(os.path.realpath(__file__))
PATH_RAW = ''
Folder='KalpaBot\\res2'
filename1 = 'translate_rawhero.json'
filename2 = 'translate_rawskill.json'
filename3 = 'modify_heroes.json'
filename4 = 'translate_rawstrategy.json'

def main():
    rawFolder = os.path.join(PATH,PATH_RAW)
    destFolder=os.path.join(PATH,Folder)
    os.makedirs(destFolder, exist_ok = True)
    data = getData(destFolder,rawFolder)

def sortdict(indict):
    data = {}
    for i in sorted(indict.keys()):
        data[i] = indict[i]
    return data

def insertpinyin(indict):
    data={}
    for i in list(indict.keys()):
        key = list(indict[i].keys())
        temp = {}
        for j in range(len(key)):
            temp[key[j]] = indict[i][key[j]]
            if key[j] == 'name':
                temp['pinyin'] = pinyin.get(indict[i]['name'],format ="strip")
                temp['nameENG'] = ''
        data[i] = temp
    return data

def getData(path,rawdata):
    print('Processing... Heroes. . .')
    hTable = json.loads(open(os.path.join(rawdata,'Heroes.json'),'rb').read())
    key = list(hTable.keys())
    field = ['source','inherent_name','star1','star2','star3','star4','star5','star6','story_name','story_content']
    skillfield = ['img','name','type','cd','cost','shoot','range','way','desc']
    #skillunlock = ['unlock_condition','inner_power1','unlock_inner1','inner_power2','unlock_inner2']
    skillunlock = ['img','unlock_condition','inner_power1','unlock_inner1','inner_power2','unlock_inner2']

    hdata = {}
    skilldata = {}
    weapondata = {}
    equipmentdata = {}
    strategydata = {}
    
    for item in key:
        hdata[item] = {fielditem:hTable[item][fielditem] for fielditem in field}
        skillmodify = []
        weaponmodify = []
        equipmodify = []
        strategymodify = []

        for i in range(0,len(hTable[item]['skill'])):
            skilldata[hTable[item]['skill'][i]['img']] = {j:hTable[item]['skill'][i][j] for j in skillfield}
            skillmodify = skillmodify + [{j:hTable[item]['skill'][i][j] for j in skillunlock}]
        hTable[item]['skill'] = skillmodify

        try:
            for i in range(0,len(hTable[item]['strategy'])):
                strategydata[hTable[item]['strategy'][i]['img']] = hTable[item]['strategy'][i]
                strategymodify = strategymodify + [hTable[item]['strategy'][i]['img']]
            hTable[item]['strategy'] = strategymodify
        except:
            hTable[item]['strategy'] = [None]
            #print('%s has no strategy.'%(item))
            continue

    skilldata = insertpinyin(skilldata)
    strategydata = insertpinyin(strategydata)
    
    print(len(skilldata))
    print(len(strategydata))

    skilldata = sortdict(skilldata)
    strategydata = sortdict(strategydata)

    open(os.path.join(path, filename1), 'wb').write(
        json.dumps(hdata, ensure_ascii=False, indent='\t').encode('utf8'))
    open(os.path.join(path, filename2), 'wb').write(
        json.dumps(skilldata, ensure_ascii=False, indent='\t').encode('utf8'))
    open(os.path.join(path, filename3), 'wb').write(
        json.dumps(hTable, ensure_ascii=False, indent='\t').encode('utf8'))
    open(os.path.join(path, filename4), 'wb').write(
        json.dumps(strategydata, ensure_ascii=False, indent='\t').encode('utf8'))

if __name__ == '__main__':
    main()

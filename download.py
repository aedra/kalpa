import os, json
import urllib.request
import urllib.parse
import requests

PATH=os.path.dirname(os.path.realpath(__file__))
filename = 'zlongame.HAR'
ajaxURL = 'http://tdj-activity.zlongame.com/tdj/data/mQuery.do?id=0&action=info&'
herohead = 'module=hero&type=basic'
herodetail = 'module=hero&type=detail&query='
accessories = 'module=ornaments&type=ornaments'

def initdownload():
    headers = {
              "name": "user-agent",
              "value": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36"
            }
    return headers

def initfile(filename):

    d = json.loads(open(filename,'rb').read())['log']['entries']
    image_url = [[item['request']['url'],item['response']['content']['size'],item['_resourceType']] for item in d if item['_resourceType'] == 'image']
    video_url = [[item['request']['url'],item['response']['content']['size'],item['_resourceType']] for item in d if item['_resourceType'] == 'media']
    
    return image_url,video_url

def getsize(url):
    d = urllib.request.urlopen(url)
    file_size = int(d.getheader('Content-Length'))
    return file_size

def downloadfile(itemList):
    cnt_1 = 0
    cnt_0 = 0

    for item in itemList:
        imgPath = item[0]
        size = getsize(imgPath)
        i_path = os.path.join(PATH,imgPath.replace('https://media.zlongame.com/',''))
        path = i_path.split('/')
        imgName = urllib.parse.unquote(path[-1])
        del path[-1]
        path = ('/').join(path)
        os.makedirs(os.path.join(path), exist_ok=True)
        i_path = os.path.join(path,imgName)

        flag = 0
        for i in range(6):

            try:
                if not os.path.isfile(i_path) or os.path.getsize(i_path) != size:
                    #req = urllib.request.Request(url=imgPath, headers=headers, method='GET')
                    #response = urllib.request.urlopen(req, timeout=40)
                    #f = open(i_path,'wb')
                    #f.write(response.read())
                    print('%s %s'%('「Loading」',imgPath))
                    r=requests.get(imgPath)
                    f=open(i_path,'wb');
                    for chunk in r.iter_content(chunk_size=255): 
                        if chunk: # filter out keep-alive new chunks
                            f.write(chunk)
                    f.close()
                    cnt_1 += 1
                else:
                    break
            except:
                continue
            else:
                flag = 1; break

        if not flag:
            print('%s %s'%('「Skip」',imgPath))
            cnt_0 +=1

    return cnt_1,cnt_0

def downloadmedia():
    try:
        imgList,videoList = initfile(filename)
        imgload,imgskip = downloadfile(imgList)
        vidload,vidskip = downloadfile(videoList)
        print('\nImages: %s Downloaded, %s Skipped.'%(imgload,imgskip))
        print('Videos: %s Downloaded, %s Skipped.'%(vidload,vidskip))
    
    except:
        print('\nHAR file not found..  skip images download')


def downloadheroes(filename,headers):
    print('\nDownload heroes data...')
    url = ajaxURL+herohead
    r = requests.get(url).json()
    heroArr = r['data']['data']
    heroid = [item['pinyin'] for item in heroArr]
    herotable = {}
    for i in heroid:
        url = ajaxURL+herodetail+i
        r = requests.get(url, headers = headers).json()
        heroInfo = r['data']['data'][0]
        herotable[i] = heroInfo

    open(os.path.join(PATH, filename), 'wb').write(
        json.dumps(herotable, ensure_ascii=False, indent='\t').encode('utf8'))
    print('Done!')

def downloadacc(filename,headers):
    print('\nDownload accessories data...')
    url = ajaxURL+accessories
    r = requests.get(url).json()
    accTable = r['data']['data']
    open(os.path.join(PATH, filename), 'wb').write(
        json.dumps(accTable, ensure_ascii=False, indent='\t').encode('utf8'))
    print('Done!')

def main():
    headers = initdownload()
    downloadmedia()
    downloadheroes('Heroes.json',headers)
    downloadacc('Accessories.json',headers)

if __name__ == '__main__':
    main()

import os
import json
import shutil

PATH=os.path.dirname(os.path.realpath(__file__))
raw = 'KalpaBot\\res2'
dest='KalpaBot\\res2_en'

def main():
    cn_data = False
    os.makedirs(dest, exist_ok=True)
    db = {}
    p_cn = os.path.join(PATH,raw)
    p_en = os.path.join(PATH,dest)

    print(os.listdir(p_cn))
    for fp in os.listdir(p_cn):
        f_cn = os.path.join(p_cn, fp)
        f_en = os.path.join(p_en, fp)
        print(f_en)
        if os.path.isfile(f_en):
            data = json.loads(open(f_en, 'rb').read())
            if os.path.isfile(f_cn):
                Merge(data, json.loads(open(f_cn, 'rb').read()))
        else:
            cn_data = True
        ###Fix BuffInfo
        if cn_data:
            print('Create new file: ',fp)
            cn_data = False #revert back to normal
            data={}
            data = json.loads(open(f_cn, 'rb').read())#use cn data instead

        #db[fp[10:-9]] = data
        open(os.path.join(dest, fp), 'wb').write(
            json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))

    open(os.path.join(dest, f_en), 'wb').write(
        json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))

def Merge(gl, cn):
	for key, item in cn.items():
		if key in gl:
			typ = type(item)
			if typ == dict:
				Merge(item, gl[key])
		else:
			gl[key] = item
	return gl

main()
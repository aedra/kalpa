import os,json
from tabulate import tabulate
import xlsxwriter

PATH=os.path.dirname(os.path.realpath(__file__))
PATH_RAW = ''
Folder='output_acc'
filename = 'MasterTable.xlsx'
textfile = 'acc.txt'

def main():

    destFolder=os.path.join(PATH,Folder)
    rawFolder = os.path.join(PATH,PATH_RAW)
    os.makedirs(destFolder, exist_ok = True)
    dest = '%s\%s'%(destFolder,filename)

    data = getData(destFolder,rawFolder)
    ToExcel(data,dest,'Dialog')


def getData(path,rawdata):
    print('Processing... Accessories. . .')
    aTable = json.loads(open('%s'%('Accessories.json'),'rb').read())
    size = len(aTable)
    partArr = {'1':'Head','2':'Body','3':'Leg','4':'Hand'}
    jobArr = ['侠客','羽士','铁卫','咒师','祝由','御风']
    typeArr = ['物攻','物防','法攻','法防','治疗效果','气血']
    
    data = []
    #tempdata = [0,0,0,0,0,0,0,0]
    #tempdata = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    tempdata = [0,0,0,0,0,None,None,None,None,None,None,None,0]

    for i in range(0,size):
        itemname = aTable[i]['name'].split('·')
        tempdata[0] = aTable[i]['icon']
        tempdata[1] = itemname[0]
        if len(itemname) == 2:
            tempdata[2] = itemname[1]
        else:
            tempdata[2] = None

        tempdata[3] = aTable[i]['quality']
        tempdata[4] = partArr[aTable[i]['position']]
#        for j in range(0,6):
#            tempdata[5+j] = aTable[i]['job'][j] if aTable[i]['job'][j] == '1' else None
#            tempdata[11+j] = aTable[i]['type'][j] if aTable[i]['type'][j] == '1' else None
#        tempdata[17] = aTable[i]['description'].replace('</br>','')
        jobcode = aTable[i]['job']
        if jobcode == '111111':
            tempdata[5] = '1'
        else:
            for j in range(0,6):
                tempdata[6+j] = aTable[i]['job'][j] if aTable[i]['job'][j] == '1' else None
        tempdata[12] = aTable[i]['description'].replace('</br>','\n')
#        else:
#            tempdata[5] = ''
#            for j in range(0,6):
#                if jobcode[j] == '1':
#                    tempdata[5] = tempdata[5] + jobArr[j] + '、'
#            tempdata[5] = tempdata[5][:-1]
#        typecode = aTable[i]['type']
#        tempdata[6] = ''
#        for j in range(0,6):
#            if typecode[j] == '1':
#                tempdata[6] = tempdata[6] + typeArr[j] + '、'
#        tempdata[6] = tempdata[6][:-1]
#        tempdata[7] = aTable[i]['description'].replace('</br>','\n')

        data = data + [tempdata]
        #tempdata = [0,0,0,0,0,0,0,0]
        #tempdata = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        tempdata = [0,0,0,0,0,None,None,None,None,None,None,None,0]

    #data = [["Icon","Name","Suffix","Rarity","Parts","Job","Type","Desc"]] + data
    #data = [["Icon","Name","Suffix","Rarity","Parts",'侠客','羽士','铁卫','咒师','祝由','御风','物攻','物防','法攻','法防','治疗效果','气血',"Desc"]] + data
    #data = [["Icon","Name","Suffix","Rarity","Parts",'All','侠客','羽士','铁卫','咒师','祝由','御风',"Desc"]] + data
    data = [["Icon","Name","Suffix","Rarity","Parts",'All']+jobArr+["Desc"]] + data
#    print(tabulate(data,headers='firstrow'))

#    with open('%s\%s'%(path,textfile),'w',encoding='utf8') as f:
#        f.write('%s'%(tabulate(diag,headers='firstrow')))

    return data,len(tempdata)

def ToExcel(data,filename,sheet):
    rowsize = len(data[0])
    colsize = data[1]
    workbook = xlsxwriter.Workbook(filename)
    #header = workbook.add_format({'text_wrap' : 'true'})
    worksheet = workbook.add_worksheet(sheet)

    for i in range(0,rowsize):
        for j in range(0,colsize):
            worksheet.write(i,j,data[0][i][j])
    workbook.close()
    return

if __name__ == '__main__':
    print('accessories main')
    main()
